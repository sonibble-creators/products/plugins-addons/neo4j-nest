# CHANGELOG.md

## 1.0.1 (unreleased)

Features:

- Fully type checking feature

Bugfixs:

## 1.0.0 (2022-03-21)

Main Release:

- Fully Support Neo4j 4.4.4
- Adding type checking and safe
- Adding some api like connection, session, write and read
