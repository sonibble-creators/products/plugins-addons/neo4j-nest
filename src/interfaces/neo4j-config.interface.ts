/**
 * ## Neo4jScheme
 *
 * The Neo4jScheme interface defines the properties of a Neo4jScheme object.
 *
 */
export type Neo4jScheme =
  | "neo4j"
  | "neo4j+s"
  | "neo4j+scc"
  | "bolt"
  | "bolt+s"
  | "bolt+scc";

/**
 * ## Neo4jConfig
 *
 * The Neo4jConfig interface defines the properties of a Neo4jConfig object.
 *
 *
 */
export interface Neo4jConfig {
  scheme: Neo4jScheme;
  host: string;
  port: number | string;
  username: string;
  password: string;
  database?: string;
}
