/**
 * ## Constants for Neo4j
 *
 * Allow of constants used in Neo4j
 */
export const NEO4J_OPTIONS = "NEO4J_OPTIONS";
export const NEO4J_DRIVER = "NEO4J_DRIVER";
